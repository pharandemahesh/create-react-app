import {takeLatest, put, call} from 'redux-saga/effects';
import axios from 'axios';



function* getNewData(){
  try{
      const payload = yield call(getData);
      yield put({type:'gotData',payload})
  } catch(e){
      yield put({type:'error',e})
  }
}

function getData(){
  axios.get('http://dummy.restapiexample.com/api/v1/employees')
  .then((response)=> response.data)
}

export default function* watchMyObject() {
  yield takeLatest('getData',getNewData);
}


