import React, { Component } from 'react';
import {connect} from 'react-redux';
import {getData} from '../redux/action';

const mapStateToProps = state =>{
  debugger
  return {aboutPageState: state.aboutPageState}
}

function mapDispatchToProps(dispatch){
  debugger
  return {
    getData : payload => dispatch(getData(payload))
  }
}

class About extends Component {
  constructor(props){
    super(props);
  }

  handleClick(){
  this.props.getData();
  }

  render() {
    return (
      <div className="about-page">
        <h1>Click below button to get rest.</h1>
        <button onClick={this.handleClick.bind(this)}>Click!</button>

      </div>
    );
  }
}



export default connect(
  mapStateToProps,
  mapDispatchToProps
)(About);

