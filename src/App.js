import React, { Component } from 'react';
import Home from './views/Home';
import About from './views/About';
import ContactUs from './views/ContactUs';
import { HashRouter as Router, Switch, Route, Link } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact component={Home}></Route>
          <Route path='/about' exact component={About}></Route>
          <Route path='/contact-us' exact component={ContactUs}></Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
