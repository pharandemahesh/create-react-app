import React, { Component } from 'react';
import {connect} from 'react-redux';
import {homePageAction1} from '../redux/action';

const mapStateToProps = state =>{
  return {homePageState: state.homepageState}
}

function mapDispatchToProps(dispatch){
  return {
    homePageAction1: payload => dispatch(homePageAction1(payload))  
  }
}

class Home extends Component {
  constructor(props){
    super();
  }

  handleClick(){
    this.props.homePageAction1(1)
    }

  render() {
    return (
      <div className="home-page">
        <h1>This is dummy app for testing deepcode plugin.</h1>
        <p>Button clicked {this.props.homePageState.clickTimes} times.</p>
        <button onClick={this.handleClick.bind(this)}>click me!</button>
      </div>
    );
  }

  componentDidMount(){
    async function f() {
      let promise = new Promise((resolve, reject) => {
        setTimeout(() => resolve("done!"), 1000)
      });
    
      let result = await promise; // wait until the promise resolves (*)
    
      alert(result); // "done!"
    }
  }

}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);